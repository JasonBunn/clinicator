﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinicator.Model;
using System.Data;
using System.Data.SqlClient;
namespace Clinicator.DBAccess
{
    /// <summary>
    /// This class will get all of the required information for patients
    /// </summary>
    class PatientDAL
    {
        /// <summary>
        /// Gets a patient by ID
        /// </summary>
        /// <param name="patientID">Selected Patient ID</param>
        /// <returns></returns>
        public static Patient getPatientByID(int patientID)
        {
            Patient patient = new Patient();

            String selectStatement = "SELECT p.id AS id, firstName, middleInitial, lastName, ssn, dob, gender, " +
                                     "address1, address2, city, p.stateCode AS stateCode, zip, phone " + //, v.datetime AS Visit "  +
                                     "FROM Patient p " +
                                     "INNER JOIN State s " +
                                        "ON p.stateCode = s.code " +
                                     "WHERE p.id = @PatientID";
             try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@PatientID", patientID);
                         using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (reader.Read())
                            {
                                patient.ID = (int)reader["id"];
                                patient.FirstName = reader["firstName"].ToString();
                                patient.LastName = reader["lastName"].ToString();
                                patient.MiddleInitial = reader["middleInitial"].ToString();
                                patient.SSN = (int)reader["ssn"];
                                patient.DOB = (DateTime)reader["dob"];
                                patient.Gender = reader["gender"].ToString();
                                patient.Address1 = reader["address1"].ToString();
                                patient.Address2 = reader["address2"].ToString();
                                patient.City = reader["city"].ToString();
                                patient.StateCode = reader["stateCode"].ToString();
                                patient.Zip = (int)reader["zip"];
                                patient.Phone = reader["phone"].ToString();
                            }
                        }
                    }
                }
            }
             catch (SqlException ex)
             {
                 throw ex;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
            return patient;
        }

        /// <summary>
        /// Gets a Patient 
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        public static List<Patient> getPatientBySearchCriteria(int patientID)
        {
            List<Patient> patientList = new List<Patient>();

            String selectStatement = " SELECT id, firstName, middleInitial, lastName, dob, gender, address1, city, stateCode, zip " +
                                     " FROM Patient " +
                                     "  WHERE id = @PatientID";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@PatientID", patientID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (reader.Read())
                            {
                                Patient patient = new Patient();
                                patient.ID = (int)reader["id"];
                                patient.FirstName = reader["firstName"].ToString();
                                patient.MiddleInitial = reader["middleInitial"].ToString();
                                patient.LastName = reader["lastName"].ToString();
                                patient.DOB = (DateTime)reader["dob"];
                                patient.Gender = reader["gender"].ToString();
                                patient.Address1 = reader["address1"].ToString();
                                patient.City = reader["city"].ToString();
                                patient.StateCode = reader["stateCode"].ToString();
                                patient.Zip = (int)reader["zip"];
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return patientList;

        }

        /// <summary>
        /// Creates a new patient
        /// </summary>
        /// <param name="visit"></param>
        /// <returns>Id of the new patient</returns>
        public static int CreatePatient(Patient patient)
        {

            string insertStatement =
                "INSERT Patient " +
                  "(ssn, firstName, middleInitial, lastName, dob, gender, address1, address2, city, stateCode, zip, phone) " +
                "VALUES (@ssn, @firstName, @middleInitial, @lastName, @dob, @gender, @address1, @address2, @city, @stateCode, @zip, @phone)";
            try
            {

                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand insertCommand = new SqlCommand(insertStatement, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@ssn", patient.SSN);
                        insertCommand.Parameters.AddWithValue("@firstName", patient.FirstName.Trim());
                        if (String.IsNullOrEmpty(patient.MiddleInitial))
                            insertCommand.Parameters.AddWithValue("@middleInitial", DBNull.Value);
                        else
                            insertCommand.Parameters.AddWithValue("@middleInitial", patient.MiddleInitial);
                        insertCommand.Parameters.AddWithValue("@lastName", patient.LastName.Trim());
                        insertCommand.Parameters.AddWithValue("@dob", patient.DOB);
                        insertCommand.Parameters.AddWithValue("@gender", patient.Gender);
                        insertCommand.Parameters.AddWithValue("@address1", patient.Address1.Trim());
                        if (String.IsNullOrEmpty(patient.Address2))
                            insertCommand.Parameters.AddWithValue("@address2", DBNull.Value);
                        else
                            insertCommand.Parameters.AddWithValue("@address2", patient.Address2.Trim());
                        insertCommand.Parameters.AddWithValue("@city", patient.City.Trim());
                        insertCommand.Parameters.AddWithValue("@stateCode", patient.StateCode);
                        insertCommand.Parameters.AddWithValue("@zip", patient.Zip);
                        insertCommand.Parameters.AddWithValue("@phone", patient.Phone);
                        insertCommand.ExecuteNonQuery();
                        string selectStatement =
                            "SELECT IDENT_CURRENT('Patient') from Patient";
                        SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                        return Convert.ToInt32(selectCommand.ExecuteScalar());
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets a list of patients that match the search attributes in the patient parameter
        /// </summary>
        /// <param name="searchPatient"></param>
        /// <returns>List of patients that match</returns>
        public static List<Patient> GetPatientsBySearchFields(Patient searchPatient)
        {
            List<Patient> patientList = new List<Patient>();
            String selectStatement;


            if (searchPatient.DOB == null)
            {
                selectStatement =
                            "SELECT id, firstName, middleInitial, lastName, dob, gender, " +
                            "address1, city, stateCode, zip " +
                            "FROM  patient " +
                            "WHERE firstName LIKE @FirstName " +
                            "AND lastName LIKE @LastName " +
                            "ORDER BY lastName, firstName, dob";
            }
            else
            {
                selectStatement =
                            "SELECT id, firstName, middleInitial, lastName, dob, gender, " +
                            "address1, city, stateCode, zip " +
                            "FROM  patient " +
                            "WHERE firstName LIKE @FirstName " +
                            "AND lastName LIKE @LastName " +
                            "AND dob = @DOB " +
                            "ORDER BY lastName, firstName, dob";
            }


            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {

                        selectCommand.Parameters.AddWithValue("@FirstName", searchPatient.FirstName + '%');
                        selectCommand.Parameters.AddWithValue("@LastName", searchPatient.LastName + '%');
                        if (searchPatient.DOB != null)
                            selectCommand.Parameters.AddWithValue("@DOB", searchPatient.DOB);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Patient patient = new Patient();
                                patient.ID = (int)reader["id"];
                                patient.FirstName = reader["firstName"].ToString();
                                patient.MiddleInitial = reader["middleInitial"].ToString();
                                patient.LastName = reader["lastName"].ToString();
                                patient.DOB = (DateTime)reader["dob"];
                                patient.Gender = reader["gender"].ToString();
                                patient.Address1 = reader["address1"].ToString();
                                patient.City = reader["city"].ToString();
                                patient.StateCode = reader["stateCode"].ToString();
                                patient.Zip = (int)reader["zip"];
                                patientList.Add(patient);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return patientList;        
        }

        /// <summary>
        /// Update patient information
        /// </summary>
        /// <param name="oldPatient">Old patient information</param>
        /// <param name="newPatient">New patient information</param>
        /// <returns>True if updated, false otherwise</returns>
        public static bool UpdatePatient(Patient oldPatient, Patient newPatient)
        {
            string updateStatement =
                  "UPDATE Patient SET " +
                  "ssn = @NewSSN, " +
                  "firstName = @NewFirstName, " +
                  "middleInitial = @NewMiddleInitial, " +
                  "lastName = @NewLastName, " +
                  "dob = @NewDOB, " +
                  "gender = @NewGender, " +
                  "address1 = @NewAddress1, " +
                  "address2 = @NewAddress2, " +
                  "city = @NewCity, " +
                  "stateCode = @NewStateCode, " +
                  "zip = @NewZip, " +
                  "phone = @NewPhone " +
                  "WHERE id = @OldID " +
                  "AND ssn = @OldSSN " +
                  "AND firstName = @OldFirstName " +
                  "AND (middleInitial = @OldMiddleInitial " +
                       "OR middleInitial IS NULL AND @OldMiddleInitial IS NULL) " +
                  "AND lastName = @OldLastName " +
                  "AND dob = @OldDOB " +
                  "AND gender = @OldGender " +
                  "AND address1 = @OldAddress1 " +
                  "AND (address2 = @OldAddress2 " +
                      "OR address2 IS NULL AND @OldAddress2 IS NULL) " +
                  "AND city = @OldCity " +
                  "AND stateCode = @OldStateCode " +
                  "AND zip = @OldZip " +
                  "AND phone = @OldPhone";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand updateCommand = new SqlCommand(updateStatement, connection))
                    {
                        updateCommand.Parameters.AddWithValue("@NewSSN", newPatient.SSN);
                        updateCommand.Parameters.AddWithValue("@NewFirstName", newPatient.FirstName);
                        if (String.IsNullOrEmpty(newPatient.MiddleInitial))
                            updateCommand.Parameters.AddWithValue("@NewMiddleInitial", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@NewMiddleInitial", newPatient.MiddleInitial);
                        updateCommand.Parameters.AddWithValue("@NewLastName", newPatient.LastName);
                        updateCommand.Parameters.AddWithValue("@NewDOB", newPatient.DOB);
                        updateCommand.Parameters.AddWithValue("@NewGender", newPatient.Gender);
                        updateCommand.Parameters.AddWithValue("@NewAddress1", newPatient.Address1);
                        if (String.IsNullOrEmpty(newPatient.Address2))
                            updateCommand.Parameters.AddWithValue("@NewAddress2", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@NewAddress2", newPatient.Address2);
                        updateCommand.Parameters.AddWithValue("@NewCity", newPatient.City);
                        updateCommand.Parameters.AddWithValue("@NewStateCode", newPatient.StateCode);
                        updateCommand.Parameters.AddWithValue("@NewZip", newPatient.Zip);
                        updateCommand.Parameters.AddWithValue("@NewPhone", newPatient.Phone);

                        updateCommand.Parameters.AddWithValue("@OldID", oldPatient.ID);
                        updateCommand.Parameters.AddWithValue("@OldSSN", oldPatient.SSN);
                        updateCommand.Parameters.AddWithValue("@OldFirstName", oldPatient.FirstName);
                        if (String.IsNullOrEmpty(oldPatient.MiddleInitial))
                            updateCommand.Parameters.AddWithValue("@OldMiddleInitial", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@OldMiddleInitial", oldPatient.MiddleInitial);
                        updateCommand.Parameters.AddWithValue("@OldLastName", oldPatient.LastName);
                        updateCommand.Parameters.AddWithValue("@OldDOB", oldPatient.DOB);
                        updateCommand.Parameters.AddWithValue("@OldGender", oldPatient.Gender);
                        updateCommand.Parameters.AddWithValue("@OldAddress1", oldPatient.Address1);
                        if (String.IsNullOrEmpty(oldPatient.Address2))
                            updateCommand.Parameters.AddWithValue("@OldAddress2", DBNull.Value);
                        else
                            updateCommand.Parameters.AddWithValue("@OldAddress2", oldPatient.Address2);
                        updateCommand.Parameters.AddWithValue("@OldCity", oldPatient.City);
                        updateCommand.Parameters.AddWithValue("@OldStateCode", oldPatient.StateCode);
                        updateCommand.Parameters.AddWithValue("@OldZip", oldPatient.Zip);
                        updateCommand.Parameters.AddWithValue("@OldPhone", oldPatient.Phone);

                        int count = updateCommand.ExecuteNonQuery();
                        if (count > 0)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks to see if patient ssn already exists in DB
        /// </summary>
        /// <param name="ssn">patient ssn to be checked for uniqueness</param>
        /// <returns>True if ssn is unique False if it already exists in DB</returns>
        public static bool IsPatientSSNUnique(int ssn)
        {
            List<int> SSNList = new List<int>();

            String selectStatement =
                "SELECT * FROM patient";

            try
            {
                using (SqlConnection connection = ClinicatorDBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SSNList.Add((int)reader["ssn"]);
                            }
                        }
                    }
                }
                for (int i = 0; i < SSNList.Count(); i++)
                {
                    if (SSNList[i] == ssn)
                        return false;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    
    }



}
