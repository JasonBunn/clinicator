﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinicator.Model;
using Clinicator.Controller;
namespace Clinicator.View
{
    /// <summary>
    /// Class for UpdateTest form in Admin form
    /// </summary>
    public partial class frmAdminUpdateTest : Form
    {
        private Test oldTest;
        private Test test;
        private AdminController adminController;

        /// <summary>
        /// Initializes the UpdateTest form
        /// </summary>
        public frmAdminUpdateTest()
        {
            InitializeComponent();
            adminController = new AdminController();
        }

        private void frmAdminUpdateTest_Load(object sender, EventArgs e)
        {
            activeComboBox.Items.Add("Y");
            activeComboBox.Items.Add("N");
            activeComboBox.SelectedIndex = -1;

            nameTextBox.Text = test.Name;
            activeComboBox.Text = test.Active;            
        }

        /// <summary>
        /// Loads the test object from Admin Test tab
        /// </summary>
        /// <param name="currentTest">The selected test to update</param>
        internal void LoadTests(Test currentTest )
        {
            test = currentTest;
            oldTest = new Test(test);   
        }

        /// <summary>
        /// Event to update a test in the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateTestSave_Click(object sender, EventArgs e)
        {       

            if (Validator.IsPresent(nameTextBox) && Validator.IsPresent(activeComboBox))
            {
                test.Name = nameTextBox.Text.ToString().Trim();
                test.Active = activeComboBox.Text.ToString();

                if (oldTest.Name.Equals(test.Name) || !Validator.IsTestNameDuplicate(nameTextBox))
                {
                    try
                    {
                        if (!adminController.updateTest(oldTest, test))
                        {
                            MessageBox.Show("Another user has updated this test or this test has been deleted.");
                        }
                        else
                        {
                            MessageBox.Show("Test was successfully updated");
                            this.DialogResult = DialogResult.OK;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
            }
        }

        private void btnUpdateTestCancel_Click(object sender, EventArgs e)
        {
            test.Name = oldTest.Name;
            test.Active = oldTest.Active;
            this.DialogResult = DialogResult.Cancel;
        }     
       
    }
}
