﻿namespace Clinicator.View
{
    partial class frmAdminUpdateTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label activeLabel;
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.activeComboBox = new System.Windows.Forms.ComboBox();
            this.btnUpdateTestSave = new System.Windows.Forms.Button();
            this.btnUpdateTestCancel = new System.Windows.Forms.Button();
            nameLabel = new System.Windows.Forms.Label();
            activeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(12, 20);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(38, 13);
            nameLabel.TabIndex = 5;
            nameLabel.Text = "Name:";
            // 
            // activeLabel
            // 
            activeLabel.AutoSize = true;
            activeLabel.Location = new System.Drawing.Point(10, 46);
            activeLabel.Name = "activeLabel";
            activeLabel.Size = new System.Drawing.Size(40, 13);
            activeLabel.TabIndex = 6;
            activeLabel.Text = "Active:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(56, 17);
            this.nameTextBox.MaxLength = 50;
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(238, 20);
            this.nameTextBox.TabIndex = 6;
            this.nameTextBox.Tag = "testName";
            // 
            // testBindingSource
            // 
            this.testBindingSource.DataSource = typeof(Clinicator.Model.Test);
            // 
            // activeComboBox
            // 
            this.activeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testBindingSource, "Active", true));
            this.activeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.activeComboBox.FormattingEnabled = true;
            this.activeComboBox.Location = new System.Drawing.Point(56, 43);
            this.activeComboBox.Name = "activeComboBox";
            this.activeComboBox.Size = new System.Drawing.Size(43, 21);
            this.activeComboBox.TabIndex = 7;
            this.activeComboBox.Tag = "testActiveCombo";
            // 
            // btnUpdateTestSave
            // 
            this.btnUpdateTestSave.Location = new System.Drawing.Point(136, 68);
            this.btnUpdateTestSave.Name = "btnUpdateTestSave";
            this.btnUpdateTestSave.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateTestSave.TabIndex = 8;
            this.btnUpdateTestSave.Text = "Save";
            this.btnUpdateTestSave.UseVisualStyleBackColor = true;
            this.btnUpdateTestSave.Click += new System.EventHandler(this.btnUpdateTestSave_Click);
            // 
            // btnUpdateTestCancel
            // 
            this.btnUpdateTestCancel.Location = new System.Drawing.Point(217, 68);
            this.btnUpdateTestCancel.Name = "btnUpdateTestCancel";
            this.btnUpdateTestCancel.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateTestCancel.TabIndex = 9;
            this.btnUpdateTestCancel.Text = "Cancel";
            this.btnUpdateTestCancel.UseVisualStyleBackColor = true;
            this.btnUpdateTestCancel.Click += new System.EventHandler(this.btnUpdateTestCancel_Click);
            // 
            // frmAdminUpdateTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(301, 97);
            this.ControlBox = false;
            this.Controls.Add(this.btnUpdateTestCancel);
            this.Controls.Add(this.btnUpdateTestSave);
            this.Controls.Add(activeLabel);
            this.Controls.Add(this.activeComboBox);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAdminUpdateTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update Test";
            this.Load += new System.EventHandler(this.frmAdminUpdateTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource testBindingSource;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.ComboBox activeComboBox;
        private System.Windows.Forms.Button btnUpdateTestSave;
        private System.Windows.Forms.Button btnUpdateTestCancel;
    }
}