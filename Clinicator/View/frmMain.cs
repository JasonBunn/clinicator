﻿using Clinicator.Controller;
using Clinicator.Model;
using Clinicator.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinicator
{
    /// <summary>
    /// Class for the Main form of the application
    /// </summary>
    public partial class frmMain : Form
    {
        private Employee employeeUser;

        /// <summary>
        /// Initializes the main form of the application
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
        }

        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmChangePassword changePasswordForm = new frmChangePassword();
            changePasswordForm.EmployeeUser = this.employeeUser;
            changePasswordForm.ShowDialog();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            login();
        }

        private void logoffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in this.MdiChildren)
            {
              childForm.Close();
            }
            stsStripRole.Text = null;
            stsEmployeeName.Text = null;
            login();
        }


        private void login()
        {
            menuStrip.Visible = false;
            frmLogin loginForm = new frmLogin();

            DialogResult result = loginForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                employeeUser = loginForm.EmployeeUser;
                loadUI();
            }
        }

        private void loadUI()
        {
            if (employeeUser.Role == "Nurse")
            {

                showMenuAndSetStatus();
                frmPatient patientForm = new frmPatient();
                patientForm.MdiParent = this;
                patientForm.Dock = DockStyle.Fill;
                patientForm.NurseLoggedIn = employeeUser;
                if (ClientSize.Width < patientForm.Size.Width || ClientSize.Height < patientForm.Size.Height)
                {
                    this.ClientSize = new Size(patientForm.Width + 25, patientForm.Height + 40);
                }
                patientForm.Show();

            } else if (employeeUser.Role == "Administrator")
            {
                showMenuAndSetStatus();
                frmAdmin adminForm = new frmAdmin();
                adminForm.MdiParent = this;
                adminForm.Dock = DockStyle.Fill;
                if (ClientSize.Width < adminForm.Size.Width || ClientSize.Height < adminForm.Size.Height)
                {
                    this.ClientSize = new Size(adminForm.Width + 25, adminForm.Height + 30);
                }

                adminForm.Show();

            }
            else
            {
                MessageBox.Show("Employee's role is not valid for access", "Invalid Role");
                login();
            }
    
        }

        private void showMenuAndSetStatus()
        {
            menuStrip.Visible = true;
            stsStripRole.Text = employeeUser.Role;
            stsEmployeeName.Text = employeeUser.FirstName + " " + employeeUser.LastName;
        }

    }
}
