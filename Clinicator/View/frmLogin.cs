﻿using Clinicator.Controller;
using Clinicator.Model;
using Clinicator.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinicator.View
{
    /// <summary>
    /// Class for the Initial form Login
    /// </summary>
    public partial class frmLogin : Form
    {

        private SecurityController securityController = new SecurityController();
        private AdminController adminController = new AdminController();

        /// <summary>
        /// Initializes the Login form
        /// </summary>
        public frmLogin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Stores the currently logged in user's data
        /// </summary>
        public Employee EmployeeUser { get; set; }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.login();
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Return)
            {
                this.login();
            }
        }

        private void login()
        {
            try
            {
                UserAccount userAccount = securityController.GetUserAccountByUserAccountIDAndPassword(txtUserName.Text.Trim(), txtPassword.Text.Trim());
                if (userAccount.EmployeeID == 0)
                    MessageBox.Show("Unable to login with user name and password entered", "Invalid Login");
                else
                {
                    EmployeeUser = adminController.GetEmployeeByID(userAccount.EmployeeID);
                    this.DialogResult = DialogResult.OK;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

    }
}
