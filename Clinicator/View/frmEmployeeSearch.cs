﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinicator.Controller;
using Clinicator.Model;

namespace Clinicator.View
{
    /// <summary>
    /// Class for the EmployeeSearch form in the Admin Form
    /// </summary>
    public partial class frmEmployeeSearch : Form
    {
        private AdminController adminController;
        public Employee employee;
        private List<Employee> employeeList;

        /// <summary>
        /// Initializes the EmployeeSearchForm
        /// </summary>
        public frmEmployeeSearch()
        {
            InitializeComponent();
            dgvEmployees.RowHeadersVisible = false;
            adminController = new AdminController();
            employee = new Employee();
            employeeList = new List<Employee>();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvEmployees_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                int rowIndex = e.RowIndex;
                employee = dgvEmployees.Rows[rowIndex].DataBoundItem as Employee;
                this.Visible = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtFirst.Text.Trim() == "" && txtLast.Text.Trim() == "" && cmbRole.Text == "")
            {
                MessageBox.Show("No search criteria has been entered", "Search Criteria Missing");
                return;
            }
            employee.FirstName = txtFirst.Text.Trim();
            employee.LastName = txtLast.Text.Trim();
            if (cmbRole.Text != "")
                employee.Role = cmbRole.Text;

            employeeList = adminController.GetEmployeesBySearchFields(employee);
            dgvEmployees.DataSource = employeeList;
            
        }

        private void frmEmployeeSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnSearch_Click(this, e);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
