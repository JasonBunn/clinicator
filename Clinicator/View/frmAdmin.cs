﻿using Clinicator.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinicator.Controller;
using Clinicator.Model;
using Microsoft.Reporting.WinForms;

namespace Clinicator
{
    /// <summary>
    /// Form class for Administrator tasks
    /// </summary>
    public partial class frmAdmin : Form
    {
        private AdminController adminController;
        private StateController stateController;
        private SecurityController securityController;
        private List<State> stateList;
        private List<Test> testList;
        private Employee employee;
        private Employee orgEmployee;
        private Test test;
        private UserAccount userAccount;

        private TabPage employeeTab;
        private TabPage testsTab;
        private TabPage reportsTab;
        frmEmployeeSearch employeeSearchForm;

        /// <summary>
        /// Initializes the Admin form object
        /// </summary>
        public frmAdmin()
        {
            InitializeComponent();
            stateController = new StateController();
            adminController = new AdminController();
            securityController = new SecurityController();
            employeeSearchForm = new frmEmployeeSearch();
            testList = new List<Test>();
            
            employeeTab = tabControlAdmin.TabPages[0];
            testsTab = tabControlAdmin.TabPages[1];
            reportsTab = tabControlAdmin.TabPages[2];

            dgvTests.RowHeadersVisible = false;

            this.ClearControls();
        }

        private void frmAdmin_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            dOBDateTimePicker.Format = DateTimePickerFormat.Custom;
            dOBDateTimePicker.CustomFormat = " ";
            setEnabledPropertyOnEmployeeControls(false);
            grpSystemAccess.Enabled = false;
            iDTextBox.Select();
            sSNMaskedTextBox.PromptChar = ' ';
            zipMaskedTextBox.PromptChar = ' ';
            dtpEnd.Value = DateTime.Now;
        }

        /// <summary>
        /// Loads the tests into the data view grid
        /// </summary>
        private void loadTestsGrid()
        {
            try
            {
                testList = adminController.GetTestList();
                dgvTests.DataSource = testList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        /// <summary>
        /// Load the form combo boxes.
        /// </summary>
        private void LoadComboBoxes()
        {
            genderComboBox.Items.Add("M");
            genderComboBox.Items.Add("F");
            genderComboBox.SelectedIndex = -1;

            activeComboBox.Items.Add("Y");
            activeComboBox.Items.Add("N");
            activeComboBox.SelectedIndex = -1;

            try
            {
                stateList = stateController.GetStateList();
                stateCodeComboBox.DataSource = stateList;
                stateCodeComboBox.DisplayMember = "code";
                stateCodeComboBox.ValueMember = "code";
                stateCodeComboBox.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

            roleComboBox.Items.Add("Doctor");
            roleComboBox.Items.Add("Nurse");
            roleComboBox.Items.Add("Administrator");
            roleComboBox.SelectedIndex = -1;

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            employeeSearchForm = new frmEmployeeSearch();
            employeeSearchForm.MdiParent = this.MdiParent;
            employeeSearchForm.Show();
        }

        /// <summary>
        /// Adds or Updates a Clinic Employee
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEmployeeSave_Click(object sender, EventArgs e)
        {
            if (Validator.IsPresent(firstNameTextBox) &&
                Validator.IsPresent(lastNameTextBox) &&
                Validator.IsPresent(genderComboBox) &&
                Validator.IsSsn(sSNMaskedTextBox) && 
                Validator.IsPresent(dOBDateTimePicker) && Validator.IsInDateRange(dOBDateTimePicker, DateTime.Now.AddYears(-150), DateTime.Now) &&
                Validator.IsPresent(address1TextBox) &&
                Validator.IsPresent(cityTextBox) && Validator.IsLettersAndSpacesOnly(cityTextBox) &&
                Validator.IsPresent(stateCodeComboBox) &&
                Validator.IsInt32(zipMaskedTextBox) && 
                Validator.IsStateZipCode(zipMaskedTextBox, 00501, 99950) &&
                Validator.IsPresent(phoneTextBox) && Validator.IsPhoneNumber(phoneTextBox) &&
                Validator.IsPresent(roleComboBox) &&
                Validator.IsPresent(activeComboBox) && IsSystemAccessValid()) 
            {
                if (employee == null || employee.ID == 0)
                {
                    populateEmployee();
                    AddEmployee();
                }
                else
                {
                    UpdateEmployee();
                }
            }
        }

        private bool IsSystemAccessValid()
        {
            if (userAccount != null && !String.IsNullOrEmpty(userAccount.Salt))
                return true;

            if ((orgEmployee != null && orgEmployee.Role.Equals(roleComboBox.SelectedItem.ToString())) || 
                 roleComboBox.SelectedItem.ToString().Equals("Doctor"))
                return true;

            if (activeComboBox.SelectedItem.ToString() == "N" || 
                (Validator.IsPresent(txtUserID) && Validator.IsMiniumumLenth(txtUserID, 5, Validator.LengthType.character) && Validator.IsUnqiueUserID(txtUserID) &&
                 Validator.IsPresent(txtPassword) && Validator.IsMiniumumLenth(txtPassword, 5, Validator.LengthType.character)))
            {
                userAccount = new UserAccount();
                userAccount.ID = txtUserID.Text.ToString().Trim();
                userAccount.Password = txtPassword.Text.ToString().Trim();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Adds and Employee to the Clinic
        /// </summary>
        private void AddEmployee()
        {
            try
            {
                if (!Validator.IsSsnUnique(employee))
                    return;
                try
                {
                    if (!String.IsNullOrEmpty(txtUserID.Text.ToString()))
                    {
                        employee.UserAccount = new UserAccount();
                        employee.UserAccount.ID = txtUserID.Text.ToString().Trim();
                        employee.UserAccount.Salt = System.Guid.NewGuid().ToString();
                        employee.UserAccount.Password = UserAccount.generateHashPassword(txtPassword.Text.Trim(), employee.UserAccount.Salt);
                    }

                    employee.ID = adminController.CreateEmployee(employee);
                }
                catch (Exception)
                {
                    
                    MessageBox.Show("Employee Registration was unsuccessful", "Database Error");
                    return;
                }
                if (employee.ID > 0)
                {
                    MessageBox.Show("Employee was successfully registered");
                    iDTextBox.Text = employee.ID.ToString();
                    this.displayEmployee();
                }
                else
                    MessageBox.Show("Employee Registration was unsuccessful", "Database Error");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        
        /// <summary>
        /// Updates Clinic Employee information.
        /// </summary>
        private void UpdateEmployee()
        {
            if (orgEmployee.SSN != employee.SSN && !Validator.IsSsnUnique(employee))
                return;
            try
            {
                if (!adminController.UpdateEmployee(orgEmployee, employee))
                {
                    MessageBox.Show("Another user has updated or deleted that Employee.", "Database Error");                       
                    iDTextBox.Text = employee.ID.ToString();
                }
                else
                {
                    MessageBox.Show("Employee was updated successfully");
                }
                this.displayEmployee();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        /// <summary>
        /// Event for Retrieve button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRetrieve_Click(object sender, EventArgs e)
        {
            grpSystemAccess.Enabled = false;
            displayEmployee();
        }

        /// <summary>
        /// Event for NewEmployee button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewEmployee_Click(object sender, EventArgs e)
        {
            iDTextBox.Text = "";
            iDTextBox.Enabled = false;
            this.ClearControls();
            employee = new Employee();
            orgEmployee = null;
            userAccount = new UserAccount();
            //employeeBindingSource.Add(employee);
            sSNMaskedTextBox.Text = "";
            zipMaskedTextBox.Text = "";
            employeeTab.Enabled = true;
            setEnabledPropertyOnEmployeeControls(true);
            btnRetrieve.Enabled = false;
            btnNewEmployee.Enabled = false;
            btnSearch.Enabled = false;
            activeComboBox.SelectedIndex = 0;
            activeComboBox.Enabled = false;
            firstNameTextBox.Focus();
        }

        /// <summary>
        /// Display Emplolyee information on form
        /// </summary>
        private void displayEmployee()
        {

            if (Validator.IsInt32(iDTextBox))
            {
                int employeeID = Convert.ToInt32(iDTextBox.Text);

                try
                {
                    employee = adminController.GetEmployeeByID(employeeID);
                    if (employee.ID == 0)
                    {
                        MessageBox.Show("No Employee Registered with that ID", "Employee Not Found");
                        return;
                    }

                    orgEmployee = new Employee(employee);
                    dOBDateTimePicker.Format = DateTimePickerFormat.Short;

                    employeeBindingSource.Clear();
                    employeeBindingSource.Add(employee);
                    if (employee.Role == "Nurse" || employee.Role == "Administrator")
                        this.showUserAcount();
                    else
                    {
                        txtUserID.Text = "";
                        txtPassword.Text = "";
                    }

                    employeeTab.Enabled = true;
                    setEnabledPropertyOnEmployeeControls(true);
                    btnRetrieve.Enabled = true;
                    btnNewEmployee.Enabled = true;
                    iDTextBox.Enabled = true;
                    roleComboBox.Enabled = false;
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        /// <summary>
        /// Resets the Employee form controls
        /// </summary>
        private void resetEmployeeForm()
        {
            iDTextBox.Enabled = true;
            this.ClearControls();
            this.setEnabledPropertyOnEmployeeControls(false);
            grpSystemAccess.Enabled = false;
            btnRetrieve.Enabled = true;
            btnSearch.Enabled = true;
            btnNewEmployee.Enabled = true;
            grpSystemAccess.Enabled = false;
        }

        /// <summary>
        /// Clears the controls in the form
        /// </summary>
        private void ClearControls()
        {
            employeeBindingSource.Clear();
            genderComboBox.SelectedIndex = -1;
            dOBDateTimePicker.Format = DateTimePickerFormat.Custom;
            dOBDateTimePicker.Checked = false;
            tabControlAdmin.SelectedTab = tabEmployees;
            txtUserID.Text = "";
            txtPassword.Text = "";
        }

        /// <summary>
        /// Set the enabled property on the registration form controls to true or false
        /// </summary>
        /// <param name="enabledValue">True if the controls should be enabled or false if they should be disabled</param>
        private void setEnabledPropertyOnEmployeeControls(bool enabledValue)
        {  
            firstNameTextBox.Enabled = enabledValue;
            firstNameTextBox.Enabled = enabledValue;
            middleInitialTextBox.Enabled = enabledValue;
            lastNameTextBox.Enabled = enabledValue;
            genderComboBox.Enabled = enabledValue;
            //sSNTextBox.Enabled = enabledValue;
            sSNMaskedTextBox.Enabled = enabledValue;
            dOBDateTimePicker.Enabled = enabledValue;
            address1TextBox.Enabled = enabledValue;
            address2TextBox.Enabled = enabledValue;
            cityTextBox.Enabled = enabledValue;
            stateCodeComboBox.Enabled = enabledValue;
            zipMaskedTextBox.Enabled = enabledValue;
            phoneTextBox.Enabled = enabledValue;
            roleComboBox.Enabled = enabledValue;
            activeComboBox.Enabled = enabledValue;

            btnEmployeeSave.Enabled = enabledValue;
            btnEmployeeCancel.Enabled = enabledValue;
        }

        /// <summary>
        /// Event for Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEmployeeCancel_Click(object sender, EventArgs e)
        {
            // if cancel the add of a new paitient, reset the entire form.
            if (this.employee.ID == 0)
            {
                resetEmployeeForm();
            }
            // Otherwise redisplay the current data for the employee
            else
            {
                displayEmployee();
            }

        }

        private void iDTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.displayEmployee();
            }
        }

        private void dOBDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            dOBDateTimePicker.Format = DateTimePickerFormat.Short;
        }

        private void handleEmployeeSearch(object sender, EventArgs e)
        {
            if (employeeSearchForm != null && employeeSearchForm.employee != null && employeeSearchForm.employee.ID > 0)
            {
                iDTextBox.Text = employeeSearchForm.employee.ID.ToString();
                employeeSearchForm.Dispose();
                this.displayEmployee();
            }            
        }

        private void roleComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
           grpSystemAccess.Enabled = IsSystemAccessAvailable();
        }

        private void showUserAcount()
        {
            try
            {
                userAccount = securityController.GetUserAccountByEmployeeID(employee.ID);
                txtUserID.Text = userAccount.ID;
                txtPassword.Text = userAccount.Password;
                if ((userAccount == null || String.IsNullOrEmpty(userAccount.ID)) && activeComboBox.SelectedText.Equals("Y"))
                    grpSystemAccess.Enabled = true;
                else
                    grpSystemAccess.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private bool IsSystemAccessAvailable()
        {
            if (roleComboBox.SelectedItem.ToString() == "Doctor")
                return false;
            else
                return true;
        }

        private void populateEmployee()
        {
            employee.FirstName = firstNameTextBox.Text.ToString().Trim();
            employee.MiddleInitial = middleInitialTextBox.Text.ToString().Trim();
            employee.LastName = lastNameTextBox.Text.ToString().Trim();
            employee.Gender = genderComboBox.SelectedItem.ToString();
            employee.SSN = Convert.ToInt32(sSNMaskedTextBox.Text.ToString().Trim());
            employee.DOB = dOBDateTimePicker.Value;
            employee.Address1 = address1TextBox.Text.ToString().Trim();
            employee.Address2 = address2TextBox.Text.ToString().Trim();
            employee.City = cityTextBox.Text.ToString().Trim();
            employee.StateCode = stateCodeComboBox.SelectedValue.ToString();
            employee.Zip = Convert.ToInt32(zipMaskedTextBox.Text.ToString().Trim());
            employee.Phone = phoneTextBox.Text.ToString().Trim();
            employee.Role = roleComboBox.SelectedItem.ToString();
            employee.Active = activeComboBox.SelectedItem.ToString();
        }

        private void tabControlAdmin_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == tabTests)
            {
                this.loadTestsGrid();
                cboTestActive.SelectedIndex = 0;
                txtSearchTest.Text = "";
                chkTestNameContains.Checked = false;
            }           
        }


        private void dgvTests_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                int rowIndex = e.RowIndex;
                Test test = dgvTests.Rows[rowIndex].DataBoundItem as Test;

                frmAdminUpdateTest adminUpdateTestForm = new frmAdminUpdateTest();
                adminUpdateTestForm.LoadTests(test);
                DialogResult result = adminUpdateTestForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.loadTestsGrid();
                }

            }
        }

        private void btnAddTest_Click(object sender, EventArgs e)
        {

            try
            {
                test = new Test();
                test.Name = txtTestName.Text.Trim();
                if (Validator.IsPresent(txtTestName) && !Validator.IsTestNameDuplicate(txtTestName) &&
                    Validator.IsMaxiumumLength(txtTestName, 50, Validator.LengthType.character))
                {
                    if (adminController.CreateTest(test) > 0)
                    {
                        txtTestName.Text = "";
                        loadTestsGrid();
                    }
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void testSearch_Click(object sender, EventArgs e)
        {
            test = new Test();
            testList = new List<Test>();
            test.Name = txtSearchTest.Text.Trim();
            if (!cboTestActive.Text.Equals("All"))
            {
                test.Active = cboTestActive.Text;
            }
            testList = adminController.searchByTestName(test, chkTestNameContains.Checked);
            dgvTests.DataSource = testList;
        }

        private void txtSearchTest_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.testSearch_Click(this, e);
            }
        }

        private void txtTestName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnAddTest_Click(this, e);
            }
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            if (Validator.IsStartBeforeEndDate(dtpBegin, dtpEnd))
            {
                try
                {
                    this.cS6232_G3DataSet.EnforceConstraints = false;
                    this.patientVisitTableAdapter.Fill(this.cS6232_G3DataSet.PatientVisit, dtpBegin.Text, dtpEnd.Text);
                    ReportParameter beginDate = new ReportParameter("BeginDate", dtpBegin.Text);
                    ReportParameter endDate = new ReportParameter("EndDate", dtpEnd.Text);
                    this.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { beginDate, endDate });
                    this.reportViewer1.RefreshReport();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void reportViewer1_ReportRefresh(object sender, CancelEventArgs e)
        {
            this.btnGenerateReport_Click(this, e);
        }

        private void iDTextBox_TextChanged(object sender, EventArgs e)
        {
            this.resetEmployeeForm();
        }

    }

}
