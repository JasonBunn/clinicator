﻿using Clinicator.Controller;
using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinicator.View
{
    /// <summary>
    /// Class for the ChangePassword form 
    /// </summary>
    public partial class frmChangePassword : Form
    {
        public Employee EmployeeUser { get; set; }
        private SecurityController securityController = new SecurityController();

        public frmChangePassword()
        {
            InitializeComponent();
        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            this.changePassword();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.changePassword();
            }
        }

        private void changePassword()
        {
            try
            {
                UserAccount userAccount = securityController.GetUserAccountByEmployeeID(EmployeeUser.ID);
                if (Validator.IsPresent(txtCurrentPassword) && Validator.IsPresent(txtNewPassword) && Validator.IsMiniumumLenth(txtNewPassword,5,Validator.LengthType.character) && Validator.IsPresent(txtRetypePassword) && Validator.IsMatchingText(txtNewPassword, txtRetypePassword))
                {
                    if (UserAccount.generateHashPassword(txtCurrentPassword.Text.Trim(), userAccount.Salt) != userAccount.Password.Trim())
                    {
                        MessageBox.Show("Current Password is invalid", "Change Password Error");
                    }
                    else
                    {
                        UserAccount newUserAccount = new UserAccount();
                        newUserAccount.Password = UserAccount.generateHashPassword(txtNewPassword.Text.Trim(), userAccount.Salt);
                        if (securityController.UpdatePassword(userAccount, newUserAccount))
                        {
                            MessageBox.Show("Password was successfully changed", "Password Changed");
                            this.DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("Password has been updated from another session", "Change Password Error");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

    }
}
