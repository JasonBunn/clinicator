﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clinicator.DBAccess;

namespace Clinicator.Model
{
    public class TestOrder
    {
        /// <summary>
        /// model class for a TestOrder object
        /// </summary>
        public TestOrder() { }

        /// <summary>
        /// Makes a duplicate instance of a patient object
        /// </summary>
        /// <param name="patient"></param>
        public TestOrder(TestOrder testOrder)
        {
            VisitID = testOrder.VisitID;
            TestID = testOrder.TestID;
            DatePerformed = testOrder.DatePerformed;
            Results = testOrder.Results;
        }
      
        public int VisitID { get; set; }

        public int TestID { get; set; }

        public string TestName { get; set; }

        public DateTime? DatePerformed { get; set; }

        public String Results { get; set; }

    }
}
