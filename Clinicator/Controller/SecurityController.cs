﻿using Clinicator.DBAccess;
using Clinicator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinicator.Controller
{
    public class SecurityController
    {

        /// <summary>
        /// Communicates with the UserAccountDAL to get the User Account information with the given
        /// userAccountID and password
        /// </summary>
        /// <param name="userAccountID">ID of userAccount to retrieve</param>
        /// <param name="password">password associated with the required userAccount</param>
        /// <returns>userAccount object with the given ID and password</returns>
        public UserAccount GetUserAccountByUserAccountIDAndPassword(String userAccountID, String password)
        {
            return UserAccountDAL.GetUserAccountByUserAccountIDAndPassword(userAccountID, password);
        }

        /// <summary>
        /// Communicates with DAL to get the User Account information for an employee ID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns>User Account Object</returns>
        public UserAccount GetUserAccountByEmployeeID(int employeeID)
        {
            return UserAccountDAL.GetUserAccountByEmployeeID(employeeID);
        }

        /// <summary>
        /// Communicates with UserAccount DAL to update the password
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="newUserAccount"></param>
        /// <returns>True if the update was successful</returns>
        public bool UpdatePassword(UserAccount userAccount, UserAccount newUserAccount)
        {
            return UserAccountDAL.UpdatePassword(userAccount, newUserAccount);
        }
    }
}
